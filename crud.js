let http = require("http");

let directory  = [
	{
		"name" : "Brandon", 
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert", 
		"email" : "jobert@mail.com"		
	}]

http.createServer(function(req,res){
	
	if(req.url == "/users" && req.method  == "GET"){
		res.writeHead(200, {"Content-Type" : "application/json"});
		res.write(JSON.stringify(directory));
		res.end();
	}

	if (req.url == "/users" && req.method == "POST"){
		
		let reqBody = "";
		req.on("data", function(data){
			reqBody += data;
		});
		req.on("end", function(){
			console.log(typeof reqBody);
			reqBody = JSON.parse(reqBody);
			let newUser = {
				"name" : reqBody.name,
				"email" : reqBody.email
			};
			directory.push(newUser);
			console.log(directory);
			res.writeHead(200, {"Content-Type" : "application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
}).listen(3000);

console.log("CRUD is now running at port 3000");